$(document).ready(function () {
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {
        hide_all();
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        hide_all();
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });  
});

//Hide all the menu fields
function hide_all() {
  var testIfVisible;
  for(var i = 1; i <= 4; i++){
    testIfVisible = document.getElementById("toggle" + i);
    if(testIfVisible.style.display == 'block'){
      testIfVisible.style.display = 'none';
    }
  }
}

//Hide all the menu fields and open the one selected
function toggle_visibility(id) {
  var toSetVisible = document.getElementById(id);
  var testIfVisible;
  for(var i = 1; i <= 4; i++){
    testIfVisible = document.getElementById("toggle" + i);
    if(toSetVisible != testIfVisible){
      if(testIfVisible.style.display == 'block'){
        testIfVisible.style.display = 'none';
      }
    }
    else{
      toSetVisible.style.display = 'block';
    }
  }
}