var colors = ["#AEDECC", "#DBFD7B", "#FDD2B5", "#F2A7A6", "#F48B94", "#7EA193", "#DBFDCB", "#2FBAA1", "#FDAA82","#F48B1F"];
class Critic {
  constructor (id, name, source, country){
    this.id = id;
    this.name = name;
    this.source = source;
    this.country = country;
    this.top10 = {};
    for(var i = 1; i <= 10; i++){
      this.top10[i-1] = "";
    }
  }
  getId (){
    return this.id;
  }
  getName (){
    return this.name;
  }
  getSource (){
    return this.source;
  }
  getCountry (){
    return this.country;
  }
  getTop10 (){
    return this.top10;
  }
  getMovieRank(movieId){
    for(var i = 0; i < 10; i++){
      if(this.top10[i+1] == movieId){
        return (i+1);
      }
    }
    return -1;
  }
  addMovie (movieId, rankMovie){
      this.top10[rankMovie] = movieId;
  }
}

class Movie {
  constructor (id, title, director, year){ //imdbid; title; director; year;
    this.id = id;
    this.title = title;
    this.director = director;
    this.year = year;
    this.votes = new Array();
    this.totalVotes = 0.0;
    this.criticsRank = {};
    for(var i = 1; i <= 10; i++){
      this.criticsRank[i] = new Array();
      this.votes[i-1] = 0;
    }
  }
  getId (){
    return this.id;
  }
  getTitle (){
    return this.title;
  }
  getDirector (){
    return this.director;
  }
  getYear (){
    return this.year;
  }
  getVotes (){
    return this.votes;
  }
  getRankVotes (rank){
    return this.votes[rank -1];
  }
  getTotalVotes (){
    return this.totalVotes;
  }
  addVote (rank){
    this.votes[rank - 1] = this.votes[rank - 1] + 1;
    this.totalVotes = this.totalVotes + 1/rank;
  }
  getCriticsRank (){
    return this.criticsRank;
  }
  getCriticsPerRank (rank){
    return this.criticsRank[rank];
  }
  addCriticId (criticId, rank){
    this.criticsRank[rank].push(criticId);
  }
  getAllCriticsIds(){
    var allCritics = new Array();
    for(var i = 0; i < 10; i++){
      for(var j = 0; j < this.criticsRank[i+1].length; j++){
        allCritics.push(this.criticsRank[i+1][j]);
      }
    }
    return allCritics;
  }
  setVotes (votesArray){
    if(votesArray.length == 10){
      this.totalVotes = 0.0;
      for(var i = 0; i < votesArray.length; i++){
        this.totalVotes = this.totalVotes + (votesArray[i]/(i+1));
        this.votes[i] = votesArray[i];
      }
    }

  }
}

class Critics {
  constructor (){
    this.critics = Array();
  }
  addCritic(critic){
    this.critics.push(critic);
  }

  addMovieToCritic(id, movieId, rank){
    var critic = this.getCritic(id);
    if(critic != null){
      critic.addMovie(movieId, rank);
    }
  }

  getCritic(id){
    for(var i = 0; i < this.critics.length; i++){
      var critic = this.critics[i];
      if (id == critic.getId()){
        return critic;
      }
    }
    return null;
  }

  getAllCritics(){
    return this.critics;
  }

  groupCritics(factor){

  }
}

class Movies {
  constructor (){
    this.movies = new Array();
  }

  addMovie(movie){
    this.movies.push(movie);

  }
  getMovie(id){
    for(var i = 0; i < this.movies.length; i++){
      var movie = this.movies[i];
      if (id == movie.getId()){
        return movie;
      }
    }
    return null;
  }
  getAllMovies(){
    return this.movies;
  }
  clearVotes (){
    var nullArray = new Array();
    for(var i =0; i < 10; i++){
      nullArray[i] = 0;
    }
    for(var i = 0; i < this.movies.length; i++){
      this.movies[i].setVotes(nullArray);
    }
  }
  getTopMoviesPerCountry (country, critics){
    this.clearVotes();
    for(var i = 0; i < this.movies.length; i++){
      var movie = this.movies[i];
      var criticsList  = movie.getAllCriticsIds();
      for(var j = 0; j < criticsList.length; j++){
        var id = criticsList[j];
        var c = critics.getCritic(id);
        if(c != null){
          var rank = c.getMovieRank(movie.getId());
          if(rank > 0){
            if(c.getCountry() == country){
              movie.addVote(rank);
            }
          }
        }
      }
    }
    return this.sortMovies();
  }
  getTopMovies (){
    this.clearVotes();
    for(var i = 0; i < this.movies.length; i++){
      var movie = this.movies[i];
      var rankings = movie.getCriticsRank();
      var votes = new Array();
      for(var j = 0; j < 10; j++){
        votes[j] = rankings[j+1].length;
        }
      movie.setVotes(votes);
    }
    return this.sortMovies();
  }
  sortMovies (){
    this.movies.sort(function(a, b) {
    return parseFloat(b.totalVotes) - parseFloat(a.totalVotes);
    });
    return this.movies;
  }
  getMovieByTitle (title){
    for(var i = 0; i < this.movies.length; i++){
      if(this.movies[i].getTitle() == title){
        return this.movies[i];
      }
    }
    return null;
  }
  getMoviesPerYear (year){
    var moviesArray = new Array();
    for(var i = 0; i < this.movies.length; i++){
      if(this.movies[i].getYear() == year){
        moviesArray.push(this.movies[i]);
      }
    }
    return moviesArray;
  }
}

class Grouping {
  constructor (factorKey){
    this.movies = new Movies();
    this.factorKey = factorKey;
    this.totalVotes = 0.0;
    this.criticsRank = new Array();
    }

  getMovies (){
    return this.movies;
  }
  getFactorKey (){
    return this.factorKey;
  }
  getCriticsRank (){
    return this.criticsRank;
  }
  getTotalVotes (){
    return this.totalVotes;
  }
  setVotes (criticsRank){
    if(criticsRank.length == 10){
      this.criticsRank = criticsRank;
      this.calculeTotalVotes();
    }
  }
  addMovie (movie){
    this.movies.addMovie(movie);
  }
  calculeTotalVotes (){
    this.totalVotes = 0.0;
    for(var i = 0; i < 10; i++){
      this.totalVotes = this.totalVotes + this.criticsRank[i]/(i+1);
    }
  }
}

class DataFetcher {
  constructor (dp){
    this.dataPath = dp;
    this.movies = new Movies();
    this.critics = new Critics();
    this.diffCountries = new Array();
    console.log("Creating new DataFetcher for data on the path " + this.dataPath);
    this.loadData("movies.csv","critics.csv");

  }
  getMovies(){
    return this.movies;
  }
  getCritics(){
    return this.critics;
  }
  loadCritics (fileName){
    var path = this.dataPath + fileName;
    var request = new XMLHttpRequest();
    request.open("GET", path, false);
    request.send(null);
    var returnValue = request.responseText;
    var textArray = returnValue.split("\n");
    for(var i = 1; i < textArray.length; i++){
        if(textArray[i].length != 0){
          var parts = textArray[i].split("; ");
          var c = new Critic(parts[0], parts[1], parts[2], parts[3]); //id, name, source, country
          this.diffCountries.push(parts[3]);
          this.critics.addCritic(c);
        }
      }
    this.diffCountries = this.diffCountries.filter(Util.Unique);
  }
  loadMovies (fileName){
    var path = this.dataPath + fileName;
    var request = new XMLHttpRequest();
    request.open("GET", path, false);
    request.send(null);
    var returnValue = request.responseText;
    var textArray = returnValue.split("\n");
    var labels = textArray[0].split("; ");
    for(var i = 1; i < textArray.length; i++){
      if(textArray[i].length != 0){
        var parts = textArray[i].split("; "); //#imdbid; title; director; year; c001; c002; ....; c177
        var m = new Movie(parts[0], parts[1], parts[2], parts[3]);
        for(var j = 4; j < parts.length; j++){
          if(parts[j].length != 0){
            var criticId = labels[j];
            this.critics.addMovieToCritic(criticId, m.getId(), parseInt(parts[j])); //criticId, movieId, rank
            m.addCriticId(criticId, parseInt(parts[j]));
          }
        }
        this.movies.addMovie(m);
      }
    }
  }
  loadData (moviesFile, criticsFile){
    this.loadCritics(criticsFile);
    this.loadMovies(moviesFile);
  }
  getCountries (){
    return this.diffCountries.sort();
  }
  getYears(){
    var moviesYears = Array();
    var allMovies = this.movies.getAllMovies();
    for(var i = 0; i < allMovies.length; i++){
      moviesYears.push(allMovies[i].getYear());
    }
    return moviesYears.filter(Util.Unique).sort();
  }
  filterMoviesPerInterval(start, end){
    if(start <= end){
      var moviesInTheInterval = new Movies();
      var allMovies = this.movies.getAllMovies();
      for(var i = 0; i < allMovies.length; i++){
        if((parseInt(allMovies[i].getYear()) >= start) && (parseInt(allMovies[i].getYear()) <= end)){
          moviesInTheInterval.addMovie(allMovies[i]);
        }
      }
      return moviesInTheInterval;
    }
    else{
      return null;
    }
  }
  groupByFactor(factor){
    if((factor == "year")||(factor == "country")){
      if(factor == "year"){
        var g = this.groupByYear();
        var array = this.setGroupingVotes(g,"year");
        return this.sortGrouping(array);

      }else {
        var g = this.groupByCountry();
        var array = this.setGroupingVotes(g,"country");
        return this.sortGrouping(array);
      }

    }else {
      return null;
    }
    }
  groupByCountry(){
    var groupings = {};
    for(var i = 0; i < this.diffCountries.length; i++){
      groupings[this.diffCountries[i]] = new Grouping(this.diffCountries[i]);
    }
    var allMovies = this.movies.getAllMovies();
    for(var i = 0; i < allMovies.length; i++){ //for each movie
      var movie = allMovies[i];
      var criticsList  = movie.getAllCriticsIds();
      for(var j = 0; j < criticsList.length; j++){
        var id = criticsList[j];
        var c = this.critics.getCritic(id);
        if(c != null){
          groupings[c.getCountry()].addMovie(movie);
        }
      }
    }
    return groupings;
  }
  groupByYear(){
    var groupings = {};
    var years = this.getYears();
    for(var i = 0; i < years.length; i++){ //initializate the grouping array
      groupings[years[i]] = new Grouping(years[i]);
    }
    var allMovies = this.movies.getAllMovies(); //get all the movies
    for(var i = 0; i < allMovies.length; i++){ //for each one
      var movie = allMovies[i];
      groupings[movie.getYear()].addMovie(movie);
    }
    return groupings;
  }
  setGroupingVotes(groupings, factor){
    for(var key in groupings){ //for each key
      var moviesList = groupings[key].getMovies().getAllMovies(); //get all the movies
      var votesPerRank = new Array(); //create our votes array
      for(var n = 0; n < 10; n++){
        votesPerRank[n] = 0;
      }
      for(var i =  0; i < moviesList.length; i++){ //for each movie
        var movie = moviesList[i];
        var movieCritics = movie.getCriticsRank();

          if(factor == "year"){
            for(var j = 0; j < 10; j++){  //for each rank of the movie
              var sumj = 0;
              for(var n = 0; n < movieCritics[j+1].length; n++){ //for each critic on that rank
                movie.addVote(j+1);  //add votes
                sumj = sumj + 1;
              }
              votesPerRank[j] = votesPerRank[j] + sumj; //sum all the critics that voted for that rank
            }
          }else if (factor == "country") {
            for(var j = 0; j < 10; j++){  //for each rank of the movie
              var sumj = 0;
              for(var n = 0; n < movieCritics[j+1].length; n++){ //for each critic on that rank
                var c = this.critics.getCritic(movieCritics[j+1][n]); //get the critic
                if(c != null){
                  if(c.getCountry() == key){ //compare the country
                    movie.addVote(j+1);  //add votes
                    sumj = sumj + 1;
                  }
                }
              }
              votesPerRank[j] = votesPerRank[j] + sumj; //sum all the critics that voted for that rank
            }
          }
      }
      groupings[key].setVotes(votesPerRank);
      groupings[key].getMovies().sortMovies();
    }
    var arrayG = new Array();
    for(var key in groupings){
      arrayG.push(groupings[key]);
    }
    return arrayG;
  }
  sortGrouping(groupings){
    groupings.sort(function(a, b) {
    return parseFloat(b.totalVotes) - parseFloat(a.totalVotes);
    });
    return groupings;
  }
  subGroupCritics(movieTitle, rank){
    var movie = this.movies.getMovieByTitle(movieTitle);
    var ranksCritics = movie.getCriticsPerRank(rank); //list of the critics that voted for the selected rank
    var groupings = {};
    for(var i = 0; i < ranksCritics.length; i++){ //for each critic
      var c = this.critics.getCritic(ranksCritics[i]);
      if(c != null){
        var country = c.getCountry();
        if(country in groupings){
          groupings[country].names.push(c.getName() + " ("+country+")");
          groupings[country].value = groupings[country].value + 1;
        } else {
          groupings[country] = {value: 1, names: new Array()};
          groupings[country].names.push(c.getName() + " ("+country+")");
        }
      }
    }
    // if length of the list is bigger than x, group countries with only one entry
    var pieChartMax = 10;
    if(Object.keys(groupings).length > pieChartMax){
      var factor = 1;
      groupings["Others"] = {value: 0, names: new Array()};
      var length = Object.keys(groupings).length;
      while(length > pieChartMax){
        var removeKeys = new Array();
        for(var key in groupings){ //get all the countries inside the factor
          if((groupings[key].value == factor)&&(key != "Others")){
            removeKeys.push(key);
            groupings["Others"].value = groupings["Others"].value + 1
            for(var i = 0; i < groupings[key].names.length; i++){ //add in the other key
              groupings["Others"].names.push(groupings[key].names[i]);
            }
          }
        }
        for(var i = 0; i < removeKeys.length; i++){ //remove the countries from the obj
          delete groupings[removeKeys[i]];
          length = length - 1;
        }
        factor = factor + 1;
      }
    }
    var arrayRep = new Array();
    var c = 0;
    for(var key in groupings){
        arrayRep.push({label: key, value: groupings[key].value, color: colors[c], names: groupings[key].names});
        c = c + 1;
    }
    return arrayRep;
  }
  subGroupMovies(year,rank){
    var movies = this.movies.getMoviesPerYear(year); //get all movies from that year
    var groupings = {};
    for(var i = 0; i < movies.length; i++){ //filter to get only movies with votes for that rank
      var votes = movies[i].getRankVotes(rank);
      if(votes > 0){
        groupings[movies[i].getTitle()] = {votes: votes, extraInfo:movies[i].getTitle() + " ("+votes + ")"};

      }
    }
    // if length of the list is bigger than x, group countries with only one entry
    var pieChartMax = 10;
    if(Object.keys(groupings).length > pieChartMax){
      var factor = 1;
      groupings["Others"] = {votes: 0, extraInfo: new Array()};
      var length = Object.keys(groupings).length;
      while(length > pieChartMax){
        var removeKeys = new Array();
        for(var key in groupings){ //get all the countries inside the factor
          if((groupings[key].votes == factor)&&(key != "Others")){
            removeKeys.push(key);
            groupings["Others"].votes = groupings["Others"].votes + 1;
            groupings["Others"].extraInfo.push(groupings[key].extraInfo);
          }
        }
        for(var i = 0; i < removeKeys.length; i++){ //remove the countries from the obj
          delete groupings[removeKeys[i]];
          length = length - 1;
        }
        factor = factor + 1;
      }
    }
    var arrayRep = new Array();
    var c = 0;
    for(var key in groupings){
        arrayRep.push({label: key, value: groupings[key].votes, color: colors[c], extraInfo: groupings[key].extraInfo});
        c = c + 1;
    }
    return arrayRep;
  }
  //We preprocess data to obtain the right format needed for drawing
  //yPosition is a rect (rectangle) argument fixed to do the drawing using d3
  preProcessDataForDrawing(moviesRanked, yPosition){
    var preprocessedMoviesDataset = Array();
    var rankingsOfAMovie = Array();
    var rankingOfAMovie = {};
    var votes;
    for(var i =  0; i < moviesRanked.length; i++){
      votes = moviesRanked[i].getVotes();
      rankingsOfAMovie = [];
      for(var j =  0; j < votes.length; j++){
        rankingOfAMovie = {};
        rankingOfAMovie["x"] = i;
        rankingOfAMovie["y"] = yPosition;
        rankingOfAMovie["w"] =  votes[j];
        rankingOfAMovie["r"] = j + 1;
        rankingsOfAMovie.push(rankingOfAMovie);
      }
      preprocessedMoviesDataset.push(rankingsOfAMovie);
    }
    return Util.transpose(preprocessedMoviesDataset);
  }

  //get the additonnal information about each movies in a specific format
  formatTheAdditionalInfo(movies){

  }

  //We preprocess the grouping data to obtain the right format needed for drawing
  //yPosition is a rect (rectangle) argument fixed to do the drawing using d3
  preProcessDataGroupingForDrawing(grouping, yPosition){
    var preprocessedGroupingDataset = Array();
    var rankings = Array();
    var aRanking = {};
    var criticsVotes;
    for(var i =  0; i < grouping.length; i++){
      criticsVotes = grouping[i].getCriticsRank();
      rankings = [];
      for(var j =  0; j < criticsVotes.length; j++){
        aRanking = {};
        aRanking["x"] = i;
        aRanking["y"] = yPosition;
        aRanking["w"] =  criticsVotes[j];
        aRanking["r"] = j + 1;
        rankings.push(aRanking);
      }
      preprocessedGroupingDataset.push(rankings);
    }
    return Util.transpose(preprocessedGroupingDataset);
  }
}

function Util(){}
Util.Unique = function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}
Util.transpose = function transpose(m) {
  return _.zip.apply(_, m);
}
