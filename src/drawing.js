function transpose(m) {
  return _.zip.apply(_, m);
}

// From Mike Bostock's tutorial on "Wrapping Long Labels".
function wrap(text, width) {
  text.each(function() {
    var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        y = text.attr("y"),
        dy = parseFloat(text.attr("dy")),
        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
    while ((word = words.pop()) !== undefined) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
      }
    }
  });
}


function draw(dataset, extraData, groupingFactor = undefined) {
  var d_x = 0;
  var _w = w * (dataset[0].length/total_movie_count);
  function viewport(){
    var e = window
    , a = 'inner';
    if ( !( 'innerWidth' in window ) )
    {
    a = 'client';
    e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
  }
  var viewport = viewport();
  var viewBox = { x: 1000, y: 0, width: viewport.width, height: viewport.height };
  var stack = d3.layout.stack();
  stack(dataset);
  dataset = transpose(dataset);

  var xStackScale = d3.scale.ordinal()
    .domain(d3.range(dataset.length))
    .rangeRoundBands([0, _w], 0.05);

  //scale for x axis
  var xScale = d3.scale.linear()
    .domain([0, d3.max(dataset,
    function(d) { return d3.max(d, function(d) { return d.w }); })])
    .range([0, xStackScale.rangeBand()]);
  var yScale = d3.scale.ordinal()
    .domain(d3.range(10).map(function(x) { return x + 1 } ))
    .rangeBands([0, h]);

  // (Re)create SVG element
  var svg = d3.select("#page-content-wrapper")//("body")
    .select("svg");
  if (svg.empty())
    svg = d3.select("#page-content-wrapper").append("svg");
  else {
    svg.remove()
    svg = d3.select("#page-content-wrapper").append("svg");
  }
  svg
    .attr("width", viewBox.width)
    .attr("height", viewBox.height)
    .attr("viewBox", "0,0," + (viewBox.width) + "," + (viewBox.height));
  var drag = d3.behavior.drag()
    .on("drag", function(d, i) {
      d_x -= d3.event.dx;
      d_x = d_x < 0 ? 0 : d_x
      d_x = d_x > (_w - viewBox.width/2) ? (_w - viewBox.width/2) : d_x
      d3.select("svg").attr("viewBox", d_x + "," + 0 + "," + viewBox.width + "," + viewBox.height);
      d3.select(".axis--y").attr("transform", "translate(" + (30 + d_x) + ", 80)");
    });
  svg.call(drag);

  var groups = svg.selectAll("g")
    .data(dataset)
    .enter()
    .append("g");

  var colorScale = d3.scale.linear().domain([1,d3.max(dataset, function(d) { return d3.max(d, function(d) { return d.w; }); })])
        .interpolate(d3.interpolateRgb)
        .range([d3.rgb("#acdbc9"), d3.rgb('#f48b94')]);


  var rects = groups.selectAll("rect")
    .data(function(d) { return d; })
    .enter()
    .append("rect")
    .attr("height", function(d) { return d.y - 2; /*return d.y - 2;*/ })
    .attr("transform", "translate(50, 80)")
    .attr("class", "votecount")
    .on("mouseover", function(d, i) {
      d3.select(this)
        .transition()
        .attr("fill", "pink");
    })
    .on("mouseout", function(d, i) {
      d3.select(this)
        .transition()
        .attr("fill", colorScale(d.w));
    })
  if (groupingFactor === undefined)
    rects.on('click', function () { if (d3.event.defaultPrevented === true) return; subvisUngrouped(d3.select(this), extraData); });
  else if (groupingFactor === 'year')
    rects.on('click', function () { if (d3.event.defaultPrevented === true) return; subvisGrouped(d3.select(this), extraData); });

  rects
    .transition()
    .delay(function(d, i) { return 150 * i ; })
    .duration(2000)
    .ease("elastic")
    .attr("x", function(d, i) { return (xStackScale(d.x) + (xStackScale.rangeBand() - xScale(d.w)) / 2); })
    .attr("y", function(d, i) { return d.y0; /*return d.y + d.y0; */})
    .attr("width", function(d) { return xScale(d.w); })
    .attr("fill", function(d) { return colorScale(d.w); })
    .each("start", function() {
      d3.select(this).attr("width", 0).attr("fill", d3.rgb("#acdbc9"));
    });
      /*
    _.defer(function() { rects.filter(function(d, i) { return d.x * 10 + i >= 70; })
      .attr("x", function(d, i) { return (xStackScale(d.x) + (xStackScale.rangeBand() - xScale(d.w)) / 2); })
      .attr("y", function(d, i) { return d.y0;})
      .attr("width", function(d) { return xScale(d.w); })
      .attr("fill", function(d) { return colorScale(d.w); });})*/


  var texts = groups
    .data(dataset).append("text")
    .attr("y", 20)
    .attr("x", 0)
    .attr("font-family", "sans-serif")
    .attr("font-size", "20px")
    .attr("text-anchor", "middle")
    .attr("class", "header-text")
    .attr("dy", 0.71 + "em")
    .attr("opacity", 0)
    .attr("transform", function(d, i) { return "translate(" + (50 + xStackScale(i) + (0.5) * (xStackScale.rangeBand())) + ", 0)" ; })
    .text(function(d, i) {
      if (extraData[d[0].x].hasOwnProperty('header'))
        t = extraData[d[0].x].header;
      else t = "";
      return t + " (" + d3.sum(d.map(function(e) { return e.w; })) + ")";
    });
  d3.selectAll(".header-text").call(wrap, xStackScale.rangeBand());
  texts
    .transition()
    .delay(function(d,i) { return 500; } )
    .duration(1000)
    //.attr("x", function(d, i) { return xStackScale(i) + (0.5) * (xStackScale.rangeBand()); })
    .attr("opacity", 1)
    .each("start", function() {
      d3.select(this)
        .attr("x", function(d,i) { return xStackScale(d[0].x); })
        .attr("opacity", 0);
    });
  var yAxis = d3.svg.axis()
    .scale(yScale)
    .orient("left");

  var xAxis = d3.svg.axis()
    .scale(xStackScale)
    .orient("bottom")
    .tickFormat(function(x) { return extraData[x].tick; });

  var yAxis_elem = svg.append("g")
    .attr("class", "axis axis--y")
    .attr("transform", "translate(30, 80)");
  yAxis_elem.transition()
    .duration(1000)
    .call(yAxis);

  var xAxis_elem = svg.append("g")
    .attr("class", "axis axis--x")
    .attr("transform", "translate(50, 600)");
  xAxis_elem.transition()
    .duration(1000)
    .call(xAxis);
  xAxis_elem
    .selectAll(".tick text")
    .call(wrap, xStackScale.rangeBand());

  }

function subvisUngrouped(info, extraData){
  showLayer("subvis");

  var movieTitle = extraData[info.data()[0].x].tick;
  var rank = info.data()[0].r;

  //get critics objs per rankings
  var rankData = getSubVisInfoUngrouped(movieTitle, rank);
  rankData.map(function(e) { e.names = e.names.join('<br />')})
  //build piechart
  d3.select("#subvisPlot").select("svg").remove();
  var pie = new d3pie("subvisPlot", {
    "header": {
      "title": {
        "text": "Per-country distribution of votes",
        "fontSize": 24
      }
    },
    "size": {
      "canvasWidth": 490,
      "pieOuterRadius": "70%"
    },
    "data": {
      "sortOrder": "value-desc",
      "content": rankData
    },
    "labels": {
		"truncation": {
			"enabled": true,
			"truncateLength": 11
		},
        "percentage": {
			"color": "#000000"
		}
	},
    "callbacks": {
      "onClickSegment": function(d) { $("#subvisNames").html(d.data.names); }
    }
  });
}

// Only grouping by year needs a sub-visualization
function subvisGrouped(info, extraData) {
  showLayer("subvis");

  var year = extraData[info.data()[0].x].tick;
  var rank = info.data()[0].r;

  var rankData = getSubVisInfoGrouped(year, rank);
  rankData.map(function(e) {
    if (e.label === "Others")
      e.extraInfo = e.extraInfo.join('<br />');
  });

  d3.select("#subvisPlot").select("svg").remove();
  var pie = new d3pie("subvisPlot", {
    "header": {
      "title": {
        "text": "Per-movie distribution of votes",
        "fontSize": 24
      }
    },
    "size": {
      "canvasWidth": 490,
      "pieOuterRadius": "70%"
    },
    "data": {
      "sortOrder": "value-desc",
      "content": rankData
    },
	"labels": {
		"truncation": {
			"enabled": true,
			"truncateLength": 11
		},
        "percentage": {
			"color": "#000000"
		}
	},
    "callbacks": {
      "onClickSegment": function(d) { $('#subvisNames').html(d.data.extraInfo); }
    }
  })
}
