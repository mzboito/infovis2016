//Populate dropdown list of countries
function populateCountriesList() {
  var select = document.getElementById("countrySelection");

  //Retrieve all the countries
  var options = df.getCountries();

  //Populating
  for (var i = 0; i < options.length; i++) {
      var opt = options[i];
      var el = document.createElement("option");
      el.textContent = opt;
      el.value = opt;
      select.appendChild(el);
  }
}

//Do the filetring
function filterByCountry() {
  var select = document.getElementById("countrySelection");
  var country = select.options[select.selectedIndex].value;

  var moviesFilteredByCountry = df.getMovies().getTopMoviesPerCountry(country, df.getCritics())

  //Updating the data viz with the new dataset
  var dataset = df.preProcessDataForDrawing(moviesFilteredByCountry, h_each);
  draw(dataset, moviesFilteredByCountry.map(function(e) { return { tick: e.title, header: e.director }; }));
  select.options[0].selected = true;
}

//Populate the 'from' dropdown list of years
function populateYearsLists() {
  var selectFromYear = document.getElementById("fromYear");

  //Retrieve all the years
  var options = df.getYears();

  //Populating
  for (var i = 0; i < options.length; i++) {
      var opt = options[i];
      var el = document.createElement("option");
      el.textContent = opt;
      el.value = opt;
      elCopy = el.cloneNode(true);
      selectFromYear.appendChild(el);
  }
}

//Empty a dropdown list
function emptyDropDownList(idDropDownList){
  var i;
  for(var i = idDropDownList.options.length - 1 ; i > 0 ; i--){
      idDropDownList.remove(i);
  }
}

//Populate the 'to' dropdown list of years
function populateToYear(valueSelected){
  var selectToYear = document.getElementById("toYear");

  //Retrieve all the years
  var options = df.getYears();

  //Get all the years which are lower than the year select in the from dropdownlist
  var indexValueSelected = options.indexOf(valueSelected);
  var finalOptions = options.slice(indexValueSelected, options.length);

  //Re-initialize the dropdown list
  emptyDropDownList(selectToYear);

  //Populating
  for (var i = 0; i < finalOptions.length; i++) {
    var opt = finalOptions[i];
    var el = document.createElement("option");
    el.textContent = opt;
    el.value = opt;
    selectToYear.appendChild(el);
  }
}

//Do the filetring by an interval of years
function filterByYear() {
  var selectFromYear = document.getElementById("fromYear");
  var fromYear = selectFromYear.options[selectFromYear.selectedIndex].value;

  var selectToYear = document.getElementById("toYear");
  var toYear = selectToYear.options[selectToYear.selectedIndex].value;

  var moviesFilteredPerIntervalRanked;

  //Retrieving the movies
  var moviesFilteredPerInterval = df.filterMoviesPerInterval(fromYear,toYear)
  //Checking if we retrive movies or not
  //(Actually it's a double checking cuz we already controle the data that we put in the in
  //the dropdown list so we are sure that there is at least one film in each interval)
  if(moviesFilteredPerInterval == null){
    alert("There is no movie which has been released in this interval : [" + fromYear + " - " + toYear + "]");
  }
  else{
    //Ranking the movies
    moviesFilteredPerIntervalRanked = moviesFilteredPerInterval.getTopMovies()
    //Updating the viz with the new dataset
    var dataset = df.preProcessDataForDrawing(moviesFilteredPerIntervalRanked, h_each);
    draw(dataset, moviesFilteredPerIntervalRanked.map(function(e) { return { tick: e.title, header: e.director }; }));
  }
  selectFromYear.options[0].selected = true;
  selectToYear.options[0].selected = true;
}

function populateGroupingList (){
  var selectFactor = document.getElementById("factor");
  var options = Array("country", "year");
  for (var i = 0; i < options.length; i++) {
    var opt = options[i];
    var el = document.createElement("option");
    el.textContent = opt;
    el.value = opt;
    selectFactor.appendChild(el);
  }
}

function restartVis (){
  var allMovies = df.getMovies().getTopMovies();
  var dataset = df.preProcessDataForDrawing(allMovies, h_each);
  draw(dataset, allMovies.map(function(e) { return { tick: e.title, header: e.director }; }));
}
//Do the grouping
function groupByFactor() {
  var selectFactor = document.getElementById("factor");
  var factorSelected = selectFactor.options[selectFactor.selectedIndex].value;

  //Retrieving the movies groups with respect to the grouping factor
  var groupsOfMovies = df.groupByFactor(factorSelected);

  if(groupsOfMovies == null){
    alert("Grouping not valid !")
  }
  else{
    //Updating the viz with the new dataset
    var dataset = df.preProcessDataGroupingForDrawing(groupsOfMovies, h_each);
    draw(dataset, groupsOfMovies.map( function(e) { return { tick: e.factorKey }; }), factorSelected);
  }
  selectFactor.options[0].selected = true;
}

//Enabling the button
function enableButton(select){
  if(select.id == "countrySelection"){
    document.getElementById("filterByCountry").disabled = false;
  }
  else if(select.id == "toYear"){
    document.getElementById("filterByYear").disabled = false;
  }
  else if(select.id == "factor"){
    document.getElementById("groupByFactor").disabled = false;
  }
}

function getSubVisInfoUngrouped(movieTitle, rank){
  return df.subGroupCritics(movieTitle, rank);
  // a sort of transformation function for the criticsVis
}

function getSubVisInfoGrouped(year, rank) {
  return df.subGroupMovies(year, rank);
}
